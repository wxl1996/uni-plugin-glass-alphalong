此项目为 uni 原生插件，为 汉捷科技的阿法龙入目智能眼镜 提供特有功能，如显示屏状态 (亮屏或灭屏) 、手电筒 、眼镜端 LED 、眼镜端连接状态 、左右眼模式 、亮度模式 、蓝牙从设备开关等功能。

---

## 注意事项

- **开发资料**

  http://www.hanjietech.cn/
  <br/>
  https://www.alphalong.cn/#/product
  <br/>
