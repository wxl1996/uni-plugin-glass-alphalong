package com.yunshu.glassmodule;

import android.app.Activity;
import android.content.ContentValues;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.alibaba.fastjson.JSONObject;

import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.bridge.UniJSCallback;
import io.dcloud.feature.uniapp.common.UniModule;

public class GlassModule extends UniModule {
    public static final Uri SCREEN_URI = Uri.parse("content://com.vidoar.launcher.settings/Screen");
    public static final Uri FLASH_LIGHT_URI = Uri.parse("content://com.vidoar.launcher.settings/FlashLight");
    public static final Uri LED_RED_URI = Uri.parse("content://com.vidoar.launcher.settings/LedRed");
    public static final Uri LED_GREEN_URI = Uri.parse("content://com.vidoar.launcher.settings/LedGreen");
    public static final Uri GLASS_STATE_URI = Uri.parse("content://com.vidoar.launcher.settings/GLASS_STATE");
    public static final Uri EYE_RIGHT_URI = Uri.parse("content://com.vidoar.launcher.settings/RightEye");
    public static final Uri RECORD_STATE_URI = Uri.parse("content://com.vidoar.launcher.settings/RecordState");
    public static final Uri BRIGHTNESS_MODE_URI = Uri.parse("content://com.vidoar.launcher.settings/BrightnessMode");
    public static final Uri BLUETOOTH_MOUDLE_URI = Uri.parse("content://com.vidoar.launcher.settings/BluetoothSwitch");
    private ContentObserver settingObserver;
    private Handler mHandler = new Handler();

    //run JS thread
    @UniJSMethod(uiThread = false)
    public void registerFlashLightObserver(final UniJSCallback onChangeCallback) {
        if (settingObserver != null) {
            unregisterFlashLightObserver();
        }
        settingObserver = new ContentObserver(mHandler) {

            @Override
            public void onChange(boolean selfChange, Uri uri) {
                JSONObject data = new JSONObject();
                data.put("selfChange", selfChange);
//                data.put("uri", uri);
                JSONObject res = getFlashLightState();
                data.put("value", res.getBoolean("value"));
                onChangeCallback.invokeAndKeepAlive(data);
                super.onChange(selfChange, uri);
            }
        };
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            mUniSDKInstance.getContext().getContentResolver().registerContentObserver(FLASH_LIGHT_URI, true, settingObserver);
        }
    }

    //run JS thread
    @UniJSMethod(uiThread = false)
    public void unregisterFlashLightObserver() {
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity && settingObserver != null) {
            mUniSDKInstance.getContext().getContentResolver().unregisterContentObserver(settingObserver);
            settingObserver = null;
        }
    }

    /**
     * 判断当前是否是右眼模式
     *
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject isRightEyeMode() {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Cursor cursor1 = mUniSDKInstance.getContext().getContentResolver().query(EYE_RIGHT_URI, null, null, null, null);
            if (cursor1 != null && cursor1.moveToFirst()) {
                int valueIndex = cursor1.getColumnIndex("data");
                String data = cursor1.getString(valueIndex);
                boolean isScreenOn = Boolean.parseBoolean(data);
                cursor1.close();
                res.put("value", isScreenOn);
                return res;
            }
        }
        res.put("msg", "Can't get setting value!");
        return res;
    }


    /**
     * 获取当前眼镜端是否接入主机端
     *
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject getGlassState() {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Cursor cursor1 = mUniSDKInstance.getContext().getContentResolver().query(GLASS_STATE_URI, null, null, null, null);
            if (cursor1 != null && cursor1.moveToFirst()) {
                int valueIndex = cursor1.getColumnIndex("data");
                String data = cursor1.getString(valueIndex);
                boolean isScreenOn = Boolean.parseBoolean(data);
                cursor1.close();
                res.put("value", isScreenOn);
                return res;
            }
        }
        res.put("msg", "Can't get setting value!");
        return res;
    }

    /**
     * 获取当前眼镜端显示屏状态
     *
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject getScreenState() {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Cursor cursor1 = mUniSDKInstance.getContext().getContentResolver().query(SCREEN_URI, null, null, null, null);
            if (cursor1 != null && cursor1.moveToFirst()) {
                int valueIndex = cursor1.getColumnIndex("data");
                String data = cursor1.getString(valueIndex);
                boolean isScreenOn = Boolean.parseBoolean(data);
                cursor1.close();
                res.put("value", isScreenOn);
                return res;
            }
        }
        res.put("msg", "Can't get setting value!");
        return res;
    }

    /**
     * 获取当前手电筒状态
     *
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject getFlashLightState() {
        Log.i("=============", "获取当前手电筒状态");
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Cursor cursor2 = mUniSDKInstance.getContext().getContentResolver().query(FLASH_LIGHT_URI, null, null, null, null);
            if (cursor2 != null && cursor2.moveToFirst()) {
                int valueIndex = cursor2.getColumnIndex("data");
                String data = cursor2.getString(valueIndex);
                boolean isFlashOn = Boolean.parseBoolean(data);
                cursor2.close();
                res.put("value", isFlashOn);
                Log.i("=============", "isFlashOn = " + isFlashOn);
                return res;
            }
        }
        res.put("msg", "Can't get setting value!");
        return res;
    }

    /**
     * 获取当前红色 led 等状态
     *
     * @return 0 关闭
     * 1 长亮
     * 2 慢闪
     * 3 快闪
     */
    @UniJSMethod(uiThread = false)
    public JSONObject getLedRedState() {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Cursor cursor3 = mUniSDKInstance.getContext().getContentResolver().query(LED_RED_URI, null, null, null, null);
            if (cursor3 != null && cursor3.moveToFirst()) {
                int valueIndex = cursor3.getColumnIndex("data");
                int data = cursor3.getInt(valueIndex);
                cursor3.close();
                res.put("value", data);
                return res;
            }
        }
        res.put("msg", "Can't get setting value!");
        return res;
    }

    /**
     * 获取当前绿色 led 等状态
     *
     * @return 0 关闭
     * 1 长亮
     * 2 慢闪
     * 3 快闪
     */
    @UniJSMethod(uiThread = false)
    public JSONObject getLedGreenState() {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Cursor cursor4 = mUniSDKInstance.getContext().getContentResolver().query(LED_GREEN_URI, null, null, null, null);
            if (cursor4 != null && cursor4.moveToFirst()) {
                int valueIndex = cursor4.getColumnIndex("data");
                int data = cursor4.getInt(valueIndex);
                cursor4.close();
                res.put("value", data);
                return res;
            }
        }
        res.put("msg", "Can't get setting value!");
        return res;
    }

    /**
     * 是否为自动亮的
     *
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject isBrightnessAuto() {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Cursor cursor1 = mUniSDKInstance.getContext().getContentResolver().query(BRIGHTNESS_MODE_URI, null, null, null, null);
            if (cursor1 != null && cursor1.moveToFirst()) {
                int valueIndex = cursor1.getColumnIndex("data");
                String data = cursor1.getString(valueIndex);
                boolean isAuto = Boolean.parseBoolean(data);
                cursor1.close();
                res.put("value", isAuto);
                return res;
            }
        }
        res.put("msg", "Can't get setting value!");
        return res;
    }

    /**
     * 蓝牙模式是否打开
     *
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject isBluetoothMoudleOpen() {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            Cursor cursor1 = mUniSDKInstance.getContext().getContentResolver().query(BLUETOOTH_MOUDLE_URI, null, null, null, null);
            if (cursor1 != null && cursor1.moveToFirst()) {
                int valueIndex = cursor1.getColumnIndex("data");
                String data = cursor1.getString(valueIndex);
                boolean isOn = Boolean.parseBoolean(data);
                cursor1.close();
                res.put("value", isOn);
                return res;
            }
        }
        res.put("msg", "Can't get setting value!");
        return res;
    }

    /**
     * 开关眼镜端显示屏
     *
     * @param isOnVal 1 开启， 0 关闭
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject setScreenState(int isOnVal) {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            boolean isOn = isOnVal == 1;
            ContentValues values = new ContentValues();
            values.put("data", isOn);
            int result = mUniSDKInstance.getContext().getContentResolver().update(SCREEN_URI, values, null, null);
            res.put("success", result == 1);
            return res;
        }
        res.put("msg", "Can't update setting!");
        return res;
    }

    /**
     * 开关眼镜端手电筒
     *
     * @param isOnVal 1 开启， 0 关闭
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject setFlashLightState(int isOnVal) {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            boolean isOn = isOnVal == 1;
            ContentValues values = new ContentValues();
            values.put("data", isOn);
            int result = mUniSDKInstance.getContext().getContentResolver().update(FLASH_LIGHT_URI, values, null, null);
            res.put("success", result == 1);
            return res;
        }
        res.put("msg", "Can't update setting!");
        return res;
    }

    /**
     * 设置眼镜端红色 led 状态
     *
     * @param state 0 关闭
     *              1 长亮
     *              2 慢闪
     *              3 快闪
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject setRedLedState(int state) {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            ContentValues values = new ContentValues();
            values.put("data", state);
            int result = mUniSDKInstance.getContext().getContentResolver().update(LED_RED_URI, values, null, null);
            res.put("success", result == 1);
            return res;
        }
        res.put("msg", "Can't update setting!");
        return res;
    }

    /**
     * 设置眼镜端红色 led 状态
     *
     * @param state 0 关闭
     *              1 长亮
     *              2 慢闪
     *              3 快闪
     */
    @UniJSMethod(uiThread = false)
    public JSONObject setGreenLedState(int state) {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            ContentValues values = new ContentValues();
            values.put("data", state);
            int result = mUniSDKInstance.getContext().getContentResolver().update(LED_GREEN_URI, values, null, null);
            res.put("success", result == 1);
            return res;
        }
        res.put("msg", "Can't update setting!");
        return res;
    }

    /**
     * 设置自动亮度开关
     *
     * @param isOnVal 1 开启， 0 关闭
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject setBrightnessAuto(int isOnVal) {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            boolean isOn = isOnVal == 1;
            ContentValues values = new ContentValues();
            values.put("data", isOn);
            int result = mUniSDKInstance.getContext().getContentResolver().update(BRIGHTNESS_MODE_URI, values, null, null);
            res.put("success", result == 1);
            return res;
        }
        res.put("msg", "Can't update setting!");
        return res;
    }

    /**
     * 设置蓝牙模块开关
     *
     * @param isOnVal 1 开启， 0 关闭
     * @return
     */
    @UniJSMethod(uiThread = false)
    public JSONObject setBluetoothMoudle(int isOnVal) {
        JSONObject res = new JSONObject();
        if (mUniSDKInstance != null && mUniSDKInstance.getContext() instanceof Activity) {
            boolean isOn = isOnVal == 1;
            ContentValues values = new ContentValues();
            values.put("data", isOn);
            int result = mUniSDKInstance.getContext().getContentResolver().update(BLUETOOTH_MOUDLE_URI, values, null, null);
            res.put("success", result == 1);
            return res;
        }
        res.put("msg", "Can't update setting!");
        return res;
    }
}
